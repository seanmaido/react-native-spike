import styled from '@emotion/native';

export const Container = styled.View`
    background-color: #fff;
    border-radius: 10px;
    shadow-color: #000;
    shadow-offset: 0px 10px;
    shadow-opacity: 0.1;
    shadow-radius: 40px;
    padding: 20px;

    ${props =>
        props.hasWarning &&
        `
        border-bottom-radius: 2px;
        border-bottom-color: #05F140;
        border-bottom-width: 2px;
    `}
`;

export const Description = styled.Text`
    color: #555;
    font-family: 'System';
    font-weight: 300;
    font-size: 16px;
    line-height: 26px;
    margin-top: 8px;
`;

export const Subtitle = styled.Text`
    color: #2cda9d;
    font-family: 'System';
    font-weight: 700;
    font-size: 14px;
    margin-top: 8px;
`;

export const Title = styled.Text`
    font-family: 'System';
    font-weight: 500;
    font-size: 24px;
`;
