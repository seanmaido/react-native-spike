import React from 'react';
import * as S from './styles';

const Card = ({description, hasWarning, subtitle, title}) => (
    <S.Container hasWarning={hasWarning}>
        <S.Title>{title}</S.Title>
        {subtitle && <S.Subtitle>{subtitle}</S.Subtitle>}
        {description && <S.Description>{description}</S.Description>}
    </S.Container>
);

export default Card;
