import React, {useState} from 'react';
import {Animated, Image} from 'react-native';

const FadeInImageWhenLoaded = ({source, style}) => {
    const [viewStyles, setViewStyles] = useState({
        opacity: new Animated.Value(0)
    });

    const handleImageLoaded = () => {
        Animated.timing(viewStyles.opacity, {
            toValue: 1,
            duration: 500
        }).start();
    };

    return (
        <Animated.View style={{opacity: viewStyles.opacity}}>
            <Image source={source} style={style} onLoad={handleImageLoaded} />
        </Animated.View>
    );
};

export default FadeInImageWhenLoaded;
