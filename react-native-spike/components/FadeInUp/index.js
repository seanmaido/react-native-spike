import React, {useEffect, useState} from 'react';
import {Animated} from 'react-native';

const FadeInUp = ({children, delay = 0}) => {
    const [viewStyles, setViewStyles] = useState({
        opacity: new Animated.Value(0),
        y: new Animated.Value(40)
    });

    const fadeIn = () => {
        Animated.timing(viewStyles.opacity, {
            toValue: 1,
            duration: 800
        }).start();
        Animated.spring(viewStyles.y, {
            friction: 10,
            tension: 1,
            toValue: 0,
            duration: 800
        }).start();
    };

    useEffect(() => {
        setTimeout(fadeIn, delay);
    }, []);

    return (
        <Animated.View
            style={{
                opacity: viewStyles.opacity,
                transform: [{translateY: viewStyles.y}]
            }}>
            {children}
        </Animated.View>
    );
};

export default FadeInUp;
