const tintColor = '#2f95dc';
const white = '#FFF';
const green = '#05F140';
const greenDark = '#2cda9d';

export default {
    white,
    tintColor,
    tabDefault: white,
    tabSelected: greenDark,
    tabBar: '#fefefe',
    errorBackground: 'red',
    errorText: white,
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: white,
    defaultViewBackground: white
};
