import React from 'react';
import {Platform, StyleSheet} from 'react-native';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import Colors from '../constants/Colors';

const config = Platform.select({
    web: {headerMode: 'screen'},
    default: {}
});

const HomeStack = createStackNavigator(
    {
        Home: HomeScreen,
        Detail: DetailScreen
    },
    config
);

HomeStack.navigationOptions = {
    tabBarLabel: 'Home',
    tabBarIcon: ({focused}) => (
        <TabBarIcon
            focused={focused}
            name={
                Platform.OS === 'ios'
                    ? `ios-information-circle${focused ? '' : '-outline'}`
                    : 'md-information-circle'
            }
        />
    )
};

HomeStack.path = '';

const LinksStack = createStackNavigator(
    {
        Links: LinksScreen
    },
    config
);

LinksStack.navigationOptions = {
    tabBarLabel: 'Links',
    tabBarIcon: ({focused}) => (
        <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'} />
    )
};

LinksStack.path = '';

const SettingsStack = createStackNavigator(
    {
        Settings: SettingsScreen
    },
    config
);

SettingsStack.navigationOptions = {
    tabBarLabel: 'Settings',
    tabBarIcon: ({focused}) => (
        <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'} />
    )
};

SettingsStack.path = '';

let bottomTabBarNavigationOptions = {
    tabBarOptions: {
        activeTintColor: Colors.tabSelected,
        inactiveTintColor: Colors.white,
        iconStyle: {},
        style: {
            backgroundColor: '#303132',
            color: '#FFF'
        }
    }
};

const tabNavigator = createBottomTabNavigator(
    {
        HomeStack,
        LinksStack,
        SettingsStack
    },
    bottomTabBarNavigationOptions
);

tabNavigator.path = '';

export default tabNavigator;
