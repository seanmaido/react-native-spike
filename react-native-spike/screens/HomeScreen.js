import * as WebBrowser from 'expo-web-browser';
import React, {useEffect, useCallback, useState} from 'react';
import {
    ActivityIndicator,
    Alert,
    Animated,
    Image,
    FlatList,
    Platform,
    ScrollView,
    SafeAreaView,
    RefreshControl,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import shuffle from 'lodash/shuffle';
import Card from '../components/Card';
import FadeInUp from '../components/FadeInUp';

export default function HomeScreen({navigation}) {
    const [data, setData] = useState(null);
    const [refreshing, setRefreshing] = useState(false);

    async function fetchData() {
        const responseJson = await fetch('https://api.punkapi.com/v2/beers?brewed_after=01-2013');
        const response = await responseJson.json();

        if (response) {
            setData(shuffle(response));
            setRefreshing(false);
        }
    }

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        setTimeout(fetchData, 1000);
    }, [refreshing]);

    const handleCardPress = (id, hasWarning) => {
        if (hasWarning) {
            Alert.alert(
                'Content warning',
                'This task has content you may not wish to see',
                [
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel'
                    },
                    {
                        text: 'I understand, continue',
                        onPress: () => {
                            navigation.navigate('Detail', {id});
                        }
                    }
                ],
                {cancelable: false}
            );
        } else {
            navigation.navigate('Detail', {id});
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView
                style={styles.container}
                contentContainerStyle={styles.contentContainer}
                refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
                <FadeInUp>
                    <Text style={[styles.heading, {paddingLeft: 20, paddingRight: 20}]}>Hello</Text>
                </FadeInUp>

                {data === null && (
                    <View style={styles.loadingContainer}>
                        <ActivityIndicator size="large" color="#999" />
                    </View>
                )}
                {data && (
                    <View style={styles.cardContainer}>
                        <FlatList
                            data={data}
                            keyExtractor={item => item.name}
                            renderItem={({item, index}) => (
                                <View key={item.name} style={{padding: 20, paddingBottom: 0}}>
                                    <FadeInUp delay={index * 150}>
                                        <TouchableOpacity
                                            onPress={() => handleCardPress(item.id, item.abv > 7)}>
                                            <Card
                                                title={item.name}
                                                description={item.tagline}
                                                subtitle={`${item.abv}%`}
                                                hasWarning={item.abv > 7}
                                            />
                                        </TouchableOpacity>
                                    </FadeInUp>
                                </View>
                            )}
                        />
                    </View>
                )}
            </ScrollView>
        </SafeAreaView>
    );
}

HomeScreen.navigationOptions = {
    header: null
};

const styles = StyleSheet.create({
    heading: {
        fontFamily: 'System',
        fontWeight: '800',
        fontSize: 35
    },
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    contentContainer: {
        paddingTop: 50
    },
    cardContainer: {
        paddingTop: 30
    },
    loadingContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
