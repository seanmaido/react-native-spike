import * as WebBrowser from 'expo-web-browser';
import React, {useEffect, useCallback, useState} from 'react';
import {
    ActivityIndicator,
    Alert,
    Animated,
    Image,
    FlatList,
    Platform,
    ScrollView,
    SafeAreaView,
    RefreshControl,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import shuffle from 'lodash/shuffle';
import Card from '../components/Card';
import FadeInUp from '../components/FadeInUp';
import FadeInImageWhenLoaded from '../components/FadeInImageWhenLoaded';

export default function DetailsScreen({navigation}) {
    const [data, setData] = useState(null);

    async function fetchData() {
        const responseJson = await fetch(
            `https://api.punkapi.com/v2/beers?ids=${navigation.state.params.id}`
        );
        const response = await responseJson.json();

        if (response) {
            setData(response[0]);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                {data === null && (
                    <View style={styles.loadingContainer}>
                        <ActivityIndicator size="large" color="#999" />
                    </View>
                )}
                {data && (
                    <View>
                        <FadeInImageWhenLoaded
                            style={{height: 300, marginBottom: 20, resizeMode: 'contain'}}
                            source={{uri: data.image_url}}
                        />

                        <FadeInUp>
                            <View style={styles.textContainer}>
                                <Text style={styles.heading}>{data.name}</Text>
                                <Text
                                    style={{
                                        color: '#2cda9d',
                                        fontSize: 16,
                                        fontWeight: '800',
                                        fontFamily: 'System'
                                    }}>
                                    {data.abv}%
                                </Text>

                                <Text
                                    style={{
                                        color: '#444',
                                        fontFamily: 'System',
                                        fontSize: 16,
                                        lineHeight: 28,
                                        marginTop: 20
                                    }}>
                                    {data.description}
                                </Text>
                            </View>
                        </FadeInUp>
                    </View>
                )}
            </ScrollView>
        </SafeAreaView>
    );
}

DetailsScreen.navigationOptions = {
    header: null
};

const styles = StyleSheet.create({
    heading: {
        fontFamily: 'System',
        fontWeight: '800',
        fontSize: 50
    },
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    contentContainer: {
        paddingTop: 20
    },
    loadingContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textContainer: {
        paddingLeft: 20,
        paddingRight: 20
    }
});
